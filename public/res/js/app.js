document.querySelectorAll('a[href^="#"]').forEach(anchor => {
    anchor.addEventListener('click', function (e) {
        e.preventDefault();

        document.querySelector(this.getAttribute('href')).scrollIntoView({
            behavior: 'smooth'
        });
    });
});

$(".carousel").swipe({
    swipe: function (event, direction, distance, duration, fingerCount, fingerData) {
        if (direction == 'left') $(this).carousel('next');
        if (direction == 'right') $(this).carousel('prev');
    },
    allowPageScroll: "vertical"
});

setTimeout(() => {
    const height = $('#myVideo').height();
    console.log(height);
    $("#thirdblock").css("height", height)
}, 4000);


var vidM = document.getElementById('myVideoMob');
vidM.addEventListener('load', () => {
    setTimeout(() => {
        var height2 = $("#myVideoMob").height();
        console.log(height2);
        $("#thirdblockMob").css("height", height2)
    }, 300);
});

$("#btnDown").click(() => {
    $('html, body').animate({
        scrollTop: $("#down").offset().top - 400
    }, 1000);
});

$("#caseID").click(() => {
    $('html, body').animate({
        scrollTop: $("#case").offset().top + 30
    }, 1000);
});
$("#advanID").click(() => {
    $('html, body').animate({
        scrollTop: $("#advan").offset().top + 30
    }, 1000);
});
$("#integrID").click(() => {
    $('html, body').animate({
        scrollTop: $("#integr").offset().top + 30
    }, 1000);
});
/***************header*button*****************/
$("#headerbtn").mouseenter(() => {
    $("#headerbtn").animate({"left": "3px", "top": "4px"}, 400);
});
$("#headerbtn").mouseleave(() => {
    $("#headerbtn").animate({"left": "0px", "top": "0px"}, 400);
});
/***************header*button*****************/


/***************first*block*button*****************/

var check1 = true;
var check2 = true;

$("#first_btn1").mouseenter(() => {
    if (check1) {
        check1 = false;
        $("#first_btn_block").addClass("first__btn__block_hov");
        $("#first_btn_block").animate({"width": "246px", "right": "200px"}, 700, () => {
            check1 = true;
            let check = $('#first_btn1').is(':hover');
            console.log(check);
            if (check === false) {
                $("#first_btn_block").animate({"width": "46px", "right": "0px"}, 700);
                $("#first_btn_text").animate({"color": "#ffffff"}, 700);
            }
            console.log('hover')
        });
        $("#first_btn_text").addClass("first__btn__text_hov");
        $("#first_btn_text").animate({"color": "#000000"}, 700);
    } else {
        return null
    }
    $("#first_btn1").css('cursor', 'pointer');
});

$("#first_btn1").mouseleave(() => {
    if (check2) {
        check2 = false;
        $("#first_btn_block").animate({"width": "46px", "right": "0px"}, 700, () => {
            check2 = true
            console.log('mouseout')
        });
        $("#first_btn_text").animate({"color": "#ffffff"}, 700);
    } else {
        return null
    }
})
;
/***************first*block*button*****************/


/***************second**button*****************/
var check3 = true;
var check4 = true;

$("#second_btn1").mouseenter(() => {
    if (check3) {
        check3 = false;
        $("#second_btn_block").addClass("first_slide_btn__block_hov");
        $("#second_btn_text").addClass("first_slide_btn__text_hov");

        $("#second_btn_block").animate({"width": "276px", "right": "230px"}, 700, () => {
            check3 = true;
            let check = $("#second_btn1").is(":hover");
            console.log(check);
            if (check === false) {
                $("#second_btn_block").animate({"width": "46px", "right": "0px"}, 700);
                $("#second_btn_text").animate({"color": "#059220"}, 700);

                //console.log('hover false')
            }
            console.log('hover')
        });
        $("#second_btn_text").animate({"color": "#000000"}, 700);
    } else {
        return null
    }
    $("#second_btn1").css('cursor', 'pointer');
});

$("#second_btn1").mouseleave(() => {
        if (check4) {
            check4 = false;
            $("#second_btn_block").animate({"width": "46px", "right": "0px"}, 700, () => {
                check4 = true
                console.log('mouseout')
            });
            $("#second_btn_text").animate({"color": "#059220"}, 700);

        } else {
            return null
        }
    }
);
/***************second**button*****************/


/***************third**button*****************/
var check5 = true;
var check6 = true;

$("#third_btn1").mouseenter(() => {
    if (check5) {
        check5 = false;
        $("#third_btn_block").addClass("second_slide_btn__block_hov");
        $("#third_btn_text").addClass("second_slide_btn__text_hov");

        $("#third_btn_block").animate({"width": "336px", "right": "290px"}, 700, () => {
            check5 = true;
            var check = $('#third_btn1').is(':hover');
            if (check === false) {
                $("#third_btn_block").animate({"width": "46px", "right": "0px"}, 700);
                $("#third_btn_text").animate({"color": "#059220"}, 700);
            }
            console.log('hover')
        });
        $("#third_btn_text").animate({"color": "#000000"}, 700);
    } else {
        return null
    }
    $("#third_btn1").css('cursor', 'pointer');
});

$("#third_btn1").mouseleave(() => {
    if (check6) {
        check6 = false;
        $("#third_btn_block").animate({"width": "46px", "right": "0px"}, 700, () => {
            check6 = true
            console.log('mouseout')
        });
        $("#third_btn_text").animate({"color": "#059220"}, 700);
    } else {
        return null
    }
});
/***************third**button*****************/


/***************fourth**button*****************/
var check7 = true;
var check8 = true;

$("#fourth_btn1").mouseenter(() => {
    if (check7) {
        check7 = false;
        $("#fourth_btn_block").addClass("first_slide_btn__block_hov");
        $("#fourth_btn_text").addClass("second_slide_btn__text_hov");

        $("#fourth_btn_block").animate({"width": "276px", "right": "230px"}, 700, () => {
            check7 = true;
            var check = $('#fourth_btn1').is(':hover');
            if (check === false) {
                $("#fourth_btn_block").animate({"width": "46px", "right": "0px"}, 700);
                $("#fourth_btn_text").animate({"color": "#059220"}, 700);
            }
            console.log('hover')
        });
        $("#fourth_btn_text").animate({"color": "#000000"}, 700);
    } else {
        return null
    }
    $("#fourth_btn1").css('cursor', 'pointer');
});

$("#fourth_btn1").mouseleave(() => {
    if (check8) {
        check8 = false;
        $("#fourth_btn_block").animate({"width": "46px", "right": "0px"}, 700, () => {
            check8 = true
            console.log('mouseout')
        });
        $("#fourth_btn_text").animate({"color": "#059220"}, 700);
    } else {
        return null
    }

});
/***************fourth**button*****************/

/***************fifth**button*****************/
var check9 = true;
var check10 = true;


$("#fifth_btn1").mouseenter(() => {
    if (check9) {
        check9 = false;
        $("#fifth_btn_block").addClass("fifth__btn__block_hov");
        $("#fifth_btn_text").addClass("second_slide_btn__text_hov");

        $("#fifth_btn_block").animate({"width": "246px", "right": "200px"}, 700, () => {
            check9 = true;
            var check = $('#fifth_btn1').is(':hover');
            if (check === false) {
                $("#fifth_btn_block").animate({"width": "46px", "right": "0px"}, 700);
                $("#fifth_btn_text").animate({"color": "#059220"}, 700);
            }
            console.log('hover')
        });
        $("#fifth_btn_text").animate({"color": "#000000"}, 700);
    } else {
        return null
    }
    $("#fifth_btn1").css('cursor', 'pointer');
});

$("#fifth_btn1").mouseleave(() => {
    if (check10) {
        check10 = false;
        $("#fifth_btn_block").animate({"width": "46px", "right": "0px"}, 700, () => {
            check10 = true
            console.log('mouseout')
        });
        $("#fifth_btn_text").animate({"color": "#059220"}, 700);
    } else {
        return null
    }
});
/***************fifth**button*****************/


/***************burger**button*****************/
$('.first-button').on('click', function () {

    $('.animated-icon1').toggleClass('open');
});
/***************burger**button*****************/




