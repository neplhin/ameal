const functions = require('firebase-functions');
const express = require('express');
const bodyParser = require('body-parser');
const path = require('path');
const nodemailer = require('nodemailer');
const xoauth2 = require('xoauth2');


const app = express();

app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());
app.set('view engine', 'ejs');
app.use(express.static(path.join(__dirname, 'public')));
app.set('views', path.join(__dirname, 'view'));


app.get('/', (req, res) => {
    res.render('index')
});

app.post('/send', (req, res) => {
    const output = `
<p>You have new request from A-Meal landing page</p>
<h3>Details:</h3>
<ul>
<li>From: ${req.body.email}</li>
<li>Subject: ${req.body.subject}</li>
</ul>
<h3>Message:</h3> 
<p>${req.body.message}</p>
`;



    var transporter = nodemailer.createTransport({
        host: 'smtp.gmail.com',
        port: 465,
        secure: true,


        auth: {
            type: 'OAuth2',
            user: "ameal.app@gmail.com",
            clientId: "514757858724-798mfvkbb3aq37jok7arhdom0rdqru7l.apps.googleusercontent.com",
            clientSecret: "l9EM0zTEa6KAMIQ8-j3n_SOX",
            refreshToken: "1/SZn-rm1F_1UM5qjhLFxGueVSW2wApa2qLSoytVN4qI0",
        }
    });

   /* var transporter = nodemailer.createTransport( {
        service: 'SendGrid',
        auth: {
            user: 'alexandergrishin',
            pass: '!Sanyok97!'
        }
    });*/


    let mailOptions = {
        to: "ameal.app@gmail.com",
        subject: "Contact Form",
        text: "Check it out",
        html: output
    };


    transporter.sendMail(mailOptions, (err, info) => {
        if (err) {
            /*console.log(err);
            transporter.close();

            return res.json({
                status: "error",
                msg: "Email sending failed"
            })*/

            res.render('index',{msg:'error'})
        }
        else {
            /*console.log(info);
            console.log("Message sent: %s", info.messageId);
            console.log("Preview URL: %s", nodemailer.getTestMessageUrl(info));
            res.redirect('/');
            transporter.close();*/
            res.render('index',{msg:'email sent'})
        }
    });

});

exports.app = functions.https.onRequest(app);










